# 8.0.32-25 更新日志

## 2024.3.11
* 更新基础镜像为 el8(rockylinux:8)。
* 升级 python 为 3.9，并复制 openssl1.1.1 的运行库，构建后的二进制包可运行于el8 及其衍生版、el9 及其衍生版、openEuler 2203。

## 2024.2.20
* 优化GreatSQL-Shell-Build，改用从服务器上下载，无需准备本地二进制文件包。

## 2024.1.22

* 版本更新到GreatSQL 8.0.32-25。
* 支持从hub.docker.com中拉取镜像并自动完成编译工作。
* 也支持调用本地脚本完成自动创建GreatSQL Shell编译环境Docker镜像，并自动创建GreatSQL Shell自动编译Docker容器，一条命令即可完成全部编译工作。
* 编译后的二进制包用xz压缩，压缩比更高，在xz压缩时采用并行方式，降低压缩耗时。

[8.0.32-25]: https://gitee.com/GreatSQL/GreatSQL-Docker/tree/greatsql-8.0.32-25/GreatSQL-Shell-Build

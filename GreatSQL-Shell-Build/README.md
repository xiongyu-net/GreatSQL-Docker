# GreatSQL-Shell-Build Docker
---
## 简介

本项目用于构建MySQL Shell for GreatSQL编译环境的本地 Docker镜像。

主要更新内容：
- 替换基础镜像为 rockylinux:8；
- 升级 python 环境为3.9；
- x86-64 架构下，编译后的二进制包可运行于 RockyLinux 8/9，openEuler 2203sp3；


## 使用

1）克隆代码库
```
cd /opt
git clone https://gitee.com/xiongyu-net/GreatSQL-Docker.git
```

2）构建本地镜像
```
docker build -t greatsql/greatsql_build8 .
```

3）运行镜像，开始编译打包
```
docker run -itd --hostname greatsql_build8 --name greatsql_build8 greatsql/greatsql_build8 bash
```

4）跟踪镜像运行日志
```
watch docker logs greatsql_build8
```


 **更多信息参考官方库 https://gitee.com/GreatSQL/GreatSQL-Docker  ** 
